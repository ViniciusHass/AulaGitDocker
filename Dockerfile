#FROM python:3.8-slim
FROM centos/python-36-centos7

## Copiando tudo que esta na pasta logal, para dentro de /app no docker
COPY . /app

## Movendo o ambiente de trabalho do docker para dentro de /app/codigo .
## Como a pasta codigo esta dentro da pasta local, eu vou em mover para dentro dela
## para poder pegar o requirements e estar na mesma pasta que o main
WORKDIR /app/codigo

RUN pip install -r ./requirements


CMD ["uvicorn", "main:app","--host", "0.0.0.0", "--port", "80", "--reload"]
