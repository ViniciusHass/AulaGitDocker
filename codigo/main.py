import pickle
from fastapi import FastAPI
import pandas as pd

app = FastAPI()
@app.post('/model')
## Coloque seu codigo na função abaixo
## Adicionar as entradas que estão faltando na chamada de função
def titanic(Sex:int, Age:float, Lifeboat:int, Pclass:int):
    with open('model/Titanic.pkl', 'rb') as fid: 
        titanic = pickle.load(fid)

        ## Eu decidi transformar os dados em uma dataframe antes de colocar no modelo
        ## Mas é possivel simplesmente colocar os dados no modelo usando 
        ## [[Age, Lifeboat, Pclass, Sex]]
        df = pd.DataFrame([[Age, Lifeboat, Pclass, Sex]], columns =['Age', 'Lifeboat', 'Pclass', 'Sex'])

        ## faço o predict
        y = titanic.predict(df)

        ## Eu vou deixar comentado um print que serve simplemente para ver qual a saida
        ## Se a saida for um vetor, tem que tirar ele da lista.
        ## Algumas pessoas tiveram numpy na saida, nesse caso é necessario transformar 
        ## em lista antes de tirar da lista.
        
        # print(y)

    ## Gero a resposta
    response = {
                "survived": bool(y[0]),
                "status": 200,  
                "message": "SUCCESS",    
               }
    return response



@app.get('/model')
def get():
    return {'hello':'test'}
